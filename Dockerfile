FROM alpine:latest

ENV HUGO_VERSION 0.48
ENV HUGO_SHA 9830ee1d51f225beb31ac2855cbc269048e1d3b75f8d5e70afe5eb44ad78f9ad

ENV GLIBC_VERSION 2.28-r0

# Install packages
RUN set -eux && \
    apk add --update --no-cache \
      ca-certificates \
      openssl \
      git \
      libstdc++ \
      nodejs \
      nodejs-npm

# Install postcss & autoprefixer
RUN npm install -g postcss-cli

# Install glibc to work with dynamically linked libraries used by extended hugo version
# See https://github.com/gohugoio/hugo/issues/4961
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
&&  wget "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/$GLIBC_VERSION/glibc-$GLIBC_VERSION.apk" \
&&  apk --no-cache add "glibc-$GLIBC_VERSION.apk" \
&&  rm "glibc-$GLIBC_VERSION.apk" \
&&  wget "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/$GLIBC_VERSION/glibc-bin-$GLIBC_VERSION.apk" \
&&  apk --no-cache add "glibc-bin-$GLIBC_VERSION.apk" \
&&  rm "glibc-bin-$GLIBC_VERSION.apk"

# Install HUGO
RUN wget -O ${HUGO_VERSION}.tar.gz https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz && \
  echo "${HUGO_SHA}  ${HUGO_VERSION}.tar.gz" | sha256sum -c && \
  tar xf ${HUGO_VERSION}.tar.gz && mv hugo* /usr/bin/hugo && \
  rm -rf  ${HUGO_VERSION}.tar.gz && \
  rm -rf /var/cache/apk/* && \
  hugo version

EXPOSE 1313

CMD ["/usr/local/bin/hugo"]
